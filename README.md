# OpenTelemac Docker - Beta version

Open-source project (GPL v3) to build [opentelemac](http://opentelemac.org/) docker images from offical sources: [http://svn.opentelemac.org/svn/opentelemac/tags/](http://svn.opentelemac.org/svn/opentelemac/tags/).

The Dockerfile is based on this documentation: [https://hub.docker.com/r/biqilong/ubuntu-telemac/](https://hub.docker.com/r/biqilong/ubuntu-telemac/)

The images are available in this [project's registry](https://gitlab.com/capedev-labs/telemac-docker/container_registry).

> **IT S A BETA VERSION, ONLY THE VERSION v7p0r1 has been checked, I'm not a Fortran expert at all :) and not sure about parallel/normal version**

# How to use the docker image

Your telemac project is stored in the folder ```/path/to/folder/containing/project/``` and the steering file is ```file.cas```

```bash
cd /path/to/folder/containing/project/
docker run --rm -v $PWD:/workdir/ registry.gitlab.com/capedev-labs/telemac-docker:v7p0r1  telemac2d /workdir/file.cas
```

The command ```docker run... telemac2d``` will run telemac2d using the file ```file.cas``` and the result files will be stored in your project folder.

Some explanations, about this command:
* ```--rm``` is a docker command to remove the container just after the end of the computation ( see docker run help)
* ```$PWD``` is your current folder :) but you change it by an absolute path
*  ```workdir``` a temporary folder 
*  ```v7p0r1``` is the version. Should be available in the [project registry](https://gitlab.com/capedev-labs/telemac-docker/container_registry)
*  ```telemac2d``` the module to run

**To sum up**
```bash
cd /<YOUR_PROJECT>/
docker run --rm -v $PWD:/workdir/ registry.gitlab.com/capedev-labs/telemac-docker:<VERSION>  <TELEMAC_MODULE> /workdir/<STEERING_FILE>
```

To display telemac help
```bash
docker run --rm registry.gitlab.com/capedev-labs/telemac-docker:<VERSION>
```

# How to launch a computation remotely (on GitLab)

It's possible to use CI/CD tools (Gitlab, Jenkins,...), to launch a computation.
For instance: [https://gitlab.com/capedev-labs/telemac-examples-bumpcri](https://gitlab.com/capedev-labs/telemac-examples-bumpcri)

Each time a file is modified and pushed to the repository, a computation is done. The pipelines are here: [https://gitlab.com/capedev-labs/telemac-examples-bumpcri/pipelines](https://gitlab.com/capedev-labs/telemac-examples-bumpcri/pipelines)


For instance here the last result:
*  The logs: [https://gitlab.com/capedev-labs/telemac-examples-bumpcri/-/jobs/147918573](https://gitlab.com/capedev-labs/telemac-examples-bumpcri/-/jobs/147918573)
*  The results file: [https://gitlab.com/capedev-labs/telemac-examples-bumpcri/-/jobs/147918573/artifacts/browse](https://gitlab.com/capedev-labs/telemac-examples-bumpcri/-/jobs/147918573/artifacts/browse)


**Warning**
*  Do no support big grid file ( at least GIT LFS to used but a PostGis should be the solution)
*  The current rule is too simple: a computation is launch for each commit. Could be easily modified ( manual launch on GitLab for instance)

# How to build your own Telemac docker image

You should follow the steps defined in the file [https://gitlab.com/capedev-labs/telemac-docker/blob/master/.gitlab-ci.yml](https://gitlab.com/capedev-labs/telemac-docker/blob/master/.gitlab-ci.yml).

## Install docker

*  Docker should be installed on your computer: https://docs.docker.com/install/
*  On windows, it should be better to install VirtualBox ( installation I currently used)

## Checkout the Telemac sources and copy file in the folder

1. Download http://svn.opentelemac.org/svn/opentelemac/tags/v7p0r1/ (in fact only the folder optionals, scripts and sources are required)
2. In the folder containing the telemac sources, copy this [project files](https://gitlab.com/capedev-labs/telemac-docker/tree/master):
   * ```Dockerfile``` 
   * ```.dockerignore``` (for performance and let Docker ignore unuseful folders for the compilation) 
   * ```metis-Makefile``` (metis makefile)
   * ```systel.cis-ubuntu-docker.cfg``` ( systel configuration, not sure about its content )
3. Modify the file ```systel.cis-ubuntu-docker.cfg``` to replace ${TELEMAC_VERSION} by your version (v7p0r1 here)
4. Launch ```docker build . -t telemac-v7p0r1``` ( will be long...)
5. Test your image:

```bash
#to print telemac help:
docker run --rm telemac-v7p0r1
#to run a computation in your project folder:
docker run --rm -v $PWD:/workdir/ telemac-v7p0r1  <TELEMAC_MODULE> /workdir/<STEERING_FILE>
```  

