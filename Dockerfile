FROM python:2.7-slim

#install required packages. TODO: maybe cmake and build-essential can be removed to have a lighter image
RUN apt-get update && \
    apt-get install -y \
      cmake \
      build-essential \
      gfortran \
      mpich && \
    rm -r /var/lib/apt/lists/* && apt-get clean

RUN pip install numpy scipy matplotlib

#See https://hub.docker.com/r/biqilong/ubuntu-telemac/ Thanks :)
#Compile metis-5.1.0
ADD optionals/metis-5.1.0 /telemac/optionals/metis-5.1.0
COPY metis-Makefile /telemac/optionals/metis-5.1.0/Makefile
RUN cd /telemac/optionals/metis-5.1.0 && make config install && mv /telemac/optionals/metis-5.1.0/build/Linux-x86_64 /telemac/metis-5.1.0

#Compile Telemac
ADD scripts /telemac/scripts
ADD sources /telemac/sources
COPY systel.cis-ubuntu-docker.cfg /telemac/systel.cis-ubuntu-docker.cfg
RUN python /telemac/scripts/python27/compileTELEMAC.py -f /telemac/systel.cis-ubuntu-docker.cfg

#clean sources not usefull to run telemac. TO be confirmed
RUN rm -rf /telemac/optionals/metis-5.1.0

#The entry point
ENTRYPOINT ["python","/telemac/scripts/python27/runcode.py","-f","/telemac/systel.cis-ubuntu-docker.cfg"]
#The default command
CMD ["-h"]
